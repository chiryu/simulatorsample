$(function(){
  $('#foo').click(function(){
    $.blockUI({'message': '読み込み中・・・'});
    setTimeout($.unblockUI, 2000);
  });
  
  // #sim-menu
  $('#sim-menu p').click(function(){
    //クリックされた#sim-menuの中のp要素に隣接する#sim-menuの中の.innerを開いたり閉じたりする。
    $(this).next('#sim-menu .inner').slideToggle();
    //クリックされた#sim-menuの中のp要素以外の#sim-menuの中のp要素に隣接する#sim-menuの中の.innerを閉じる
    $('#sim-menu p').not($(this)).next('#sim-menu .inner').slideUp();
  });
});
